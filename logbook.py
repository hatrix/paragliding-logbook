#!/usr/bin/env python3
from collections import Counter
from jinja2 import Environment, FileSystemLoader
from time import strptime, strftime
import bs4 as bs
import calendar
import argparse
import os.path
import json
import copy
import re


def init_logfile(logfile):
    if os.path.isfile(logfile):
        return
    else:
        with open(logfile, 'w') as fp:
            json.dump([], fp)


def log_flight(logfile):
    def get_field(name, common):
        if common:
            spaces = ' ' * (29 - (2 + len(common) + 15))
            print('{:15s}{}({}): '.format(name, spaces, common), end='')
        else:
            print('{:29s}: '.format(name), end='')
        field = input()

        if common and not field:
            return common
        return field

    def get_common(field, data):
        if data:
            return sorted(Counter(j[field] for j in data).items(), 
                          key=lambda x: x[1])[-1][0]
        else:
            return None

    def get_common_canopy(data):
        canopies = {}
        for flight in data:
            if flight['canopy'] not in canopies:
                canopies[flight['canopy']] = 0
            canopies[flight['canopy']] += int(flight['duration'])

        max_flown = max(canopies.items(), key=lambda x: x[1])
        return max_flown[0]
    
    with open(logfile, 'r') as fp:
        data = json.load(fp)
    flight = {}

    # Get most common values
    common_loc = get_common('location', data)
    common_take_off = get_common('take_off', data)
    common_landing = get_common('landing', data)
            
    # For the canopy, we'll use the hours instead of the number
    # of the number of flights
    common_canopy = get_common_canopy(data)

    flight['number'] = get_field('Number', None)
    if len(flight['number']) == 0 or flight['number'] == '':
        return False

    flight['date'] = get_field('Date (ddmmyy)', common=None)
    flight['location'] = get_field('Location', common_loc)
    flight['take_off'] = get_field('Take Off', common_take_off)
    flight['landing'] = get_field('Landing', common_landing)
    flight['duration'] = get_field('Duration', common=None)
    flight['elevation'] = get_field('Elevation', common=None)
    flight['canopy'] = get_field('Canopy', common_canopy)
    flight['notes'] = get_field('Notes', common=None)
    flight['youtube'] = get_field('Youtube', common=None)
    flight['gpx'] = get_field('Track', common=None)
    
    # Allow entering new lines with \n
    flight['notes'] = flight['notes'].replace('\\n', '\n')

    # flight already exists
    if flight in data:
        print('flight already exists in logbook -> not added\n')
        return True

    data.append(flight)
    print('\n')
    with open(logfile, 'w') as fp:
        json.dump(sorted(data, key=lambda x: int(x['number'])), 
                  fp,
                  sort_keys=True,
                  ensure_ascii=False,
                  indent=4)

    return True


def loop_log(logfile):
    while True:
        if not log_flight(logfile):
            break


def make_stats(logfile, display=True):
    def get_numbers(field, data):
        field_num = sorted(Counter(j[field] for j in data).items(), 
                    key=lambda x: x[0], reverse=True)

        return field_num


    def get_total_flight_time(data):
        res = [sum([int(f['duration']) for f in data]), 0]
        res[1] = "{:02}".format(res[0] % 60)
        res[0] = res[0] // 60
        return res

    def get_flight_time_for_field(data, field):
        res = {}
        for flight in data:
            if field not in res:
                res[field] = {}

            if flight[field] not in res[field]:
                res[field][flight[field]] = 0

            res[field][flight[field]] += int(flight['duration'])
        return res

    with open(logfile, 'r') as fp:
        data = json.load(fp)

    fields = ['location', 'take_off', 'landing', 'canopy']

    res = {}
    times = {}
    for field in fields:
        numbers = get_numbers(field, data)
        res[field] = numbers

        times.update(get_flight_time_for_field(data, field))

        # Debug
        if display:
            print(field.title())
            print('-' * 24)
            for value, count in numbers:
                print('  {:10s} {} flights'.format(value, count))
            print('')


    # Add the duration for each field
    new_data = {}
    for field, values in res.items():
        new_data[field] = []
        for item in values:
            formatted_duration = [0, "{:02}".format(times[field][item[0]] % 60)]
            formatted_duration[0] = times[field][item[0]] // 60
            new_data[field].append((item[0], item[1], formatted_duration))

    # Hours, minutes
    new_data['flight_time'] = get_total_flight_time(data)

    return new_data


def render_template(filename, out_fname, data):
    env = Environment(loader=FileSystemLoader('templates'))
    template = env.get_template(filename)
    rendered = template.render(**data)

    out = open(out_fname, 'w')
    out.write(rendered)


def get_videos(data):
    flights_video = copy.deepcopy(data)
    flights_video = [j for j in flights_video if 'youtube' in j.keys()]
    for j in flights_video:
        if 'youtube' in j['youtube']:
            reg = r"http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\\" \
                  r".be\/)([\w\-\_]*)(&(amp;)?​[\w\?​=]*)?"
            sub = r'<div class="video_sup">' \
                  r'<div class="video_container">' \
                  r'<iframe class="video"' \
                  r'allowfullscreen="allowfullscreen"' \
                  r'src="https://www.youtube.com/embed/\1"></iframe>' \
                  r'</div>' \
                  r'</div>'
            j['youtube'] = re.sub(reg, sub, j['youtube'])
    return flights_video


def make_calendar(data):
    dates = {}
    for flight in data:
        if flight['date'] not in dates:
            dates[flight['date']] = 1
        else:
            dates[flight['date']] += 1

    days = [(word[:3], word[0]) for word in calendar.day_name]
    different_years = sorted(set([int(y.split('-')[0]) for y in dates]),
                             reverse=True)

    number_flights_per_year = { year: 0 for year in different_years }
    number_flights_per_month = {}

    for date, number in dates.items():
        number_flights_per_year[int(date.split('-')[0])] += number

        if date[:-3] not in number_flights_per_month.keys():
            number_flights_per_month[date[:-3]] = 0
        number_flights_per_month[date[:-3]] += number
    
    result = []
    # Loop through every year
    for year in different_years:
        c = calendar.HTMLCalendar(calendar.MONDAY)
        html = c.formatyear(year)
        html = bs.BeautifulSoup(html, 'lxml') # html of year
        
        # Replace day names by their first letter
        for day, letter in days:
            texts = html.find_all(text=day)
            for t in texts:
                new_text = str(t).replace(day, letter)
                t.replace_with(new_text)

        # Add total number of flights under year's number
        # fuck, that's hard to do with this library
        br = html.new_tag('br')
        year_td = html.find(text=year).parent
        
        year_td.string = ''
        year_td.append(bs.NavigableString(str(year)))
        year_td.append(br)

        nb_j = str(number_flights_per_year[year]) + ' flights'
        year_td.append(bs.NavigableString(nb_j))
        
        # Add color for day containing a flight
        for date in dates.keys():
            y = int(date.split('-')[0])
            m = int(date.split('-')[1])
            d = int(date.split('-')[2])

            if y != year:
                continue
            
            html_month = html.find(text=calendar.month_name[m])\
                             .parent.parent.parent
            flight_day = html_month.find(text=d).parent
        
            # Add number of flights
            month_tag = html.find('th', {'class': 'month'}, text=calendar.month_name[m])
            if month_tag:
                br = html.new_tag('br')
                month_name = month_tag.text
                month_tag.string = ''
                month_tag.append(bs.NavigableString(month_name))
                month_tag.append(br)
                nb_j = str(number_flights_per_month[date[:-3]]) + ' flights'
                month_tag.append(bs.NavigableString(nb_j))
            
            if dates[date] >= 4:
                color = 'red'
            elif dates[date] >= 3:
                color = 'orange'
            elif dates[date] == 2:
                color = 'yellow'
            else: # one flight
                color = 'green'
            flight_day['class'] += ['color-' + color]

        result.append(html)

    return result


def get_licenses(logfile):
    with open(logfile, 'r') as fp:
        try:
            licenses = json.load(fp)
        except:
            licenses = []

    for l in licenses:
        date = strptime(l['date'], '%d%m%y')
        l['date'] = strftime('%Y-%m-%d', date)

    return sorted(licenses, key=lambda x: x['date'])


def write_licenses(logfile):
    with open(logfile, 'r') as fp:
        try:
            licenses = json.load(fp)
        except:
            licenses = []

    while True:
        l = {}
        l['name'] = input('License: ') 
        if l['name'] == '':
            break
        l['date'] = input('Date (ddmmyy): ')
        l['number'] = input('Number: ')
        l['loc'] = input('Location: ')
        licenses.append(l)

    with open(logfile, 'w') as fp:
        json.dump(sorted(licenses, key=lambda x: x['name']), 
                  fp,
                  indent=4)


def make_html(logfile):
    with open(logfile, 'r') as fp:
        data = json.load(fp)
    
    with open('licenses.json', 'r') as fp:
        licenses = json.load(fp)

    for flight in data:
        date = strptime(flight['date'], '%d%m%y')
        flight['date'] = strftime('%Y-%m-%d', date)

    data = data[::-1]  # last flights first

    flights_video = get_videos(data)
    stats = make_stats(logfile, display=False)
    years_calendar = make_calendar(data)
    licenses = get_licenses('licenses.json')

    render_template('index.html', 
                    'index.html', 
                    {'flights': data})
    render_template('stats.html', 
                    'stats.html', 
                    {'stats': stats})
    render_template('video.html', 
                    'video.html', 
                    {'flights': flights_video})
    render_template('calendar.html', 
                    'calendar.html', 
                    {'years': years_calendar})
    render_template('licenses.html', 
                    'licenses.html', 
                    {'licenses': licenses})
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=str, default='flights.json')
    parser.add_argument('--html', action='store_true', 
                        default=False)
    parser.add_argument('--license', action='store_true', 
                        default=False)
    args = parser.parse_args()

    if args.html:
        make_html(args.file)
        exit()

    if args.license:
        write_licenses('licenses.json')
        exit()

    init_logfile(args.file)
    loop_log(args.file)
